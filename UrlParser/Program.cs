﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace Otus.RegularExpressions
{
    class Program
    {
        static readonly string _pattern = @"https?:\/\/([^-][(\w-)]+\.)+[a-zA-Z]+(\/([^\s'""<>\.;,:\(\)]|[\.,:;](?=\S))+)?";

        static void Main()
        {
            Console.WriteLine("Введите URL:");
            var url = Console.ReadLine();
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                Console.WriteLine("Неверно задан URL");
                return;
            }
            using (var client = new WebClient())
            {
                try
                {
                    Console.WriteLine("Загрузка...");
                    var html = client.DownloadString(url);
                    var urlMatches = Regex.Matches(html, _pattern);
                    if (urlMatches.Count == 0)
                    {
                        Console.WriteLine("Ничего не найдено");
                        return;
                    }
                    Console.WriteLine($"Найдено {urlMatches.Count} url:");
                    foreach (var match in urlMatches)
                    {
                        Console.WriteLine(match);
                    }
                }
                catch (WebException e)
                {
                    Console.WriteLine($"Ошибка при попытке подключения к {url}: {e.Message}");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
